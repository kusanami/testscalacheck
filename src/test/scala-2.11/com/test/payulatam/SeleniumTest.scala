package com.test.payulatam


import java.util.{Date, Random}
import java.util.concurrent.TimeUnit


import org.junit.runner.RunWith
import org.openqa.selenium.{By, WebDriver}
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.support.ui.{ExpectedConditions, WebDriverWait}
import org.scalacheck.Gen
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{Matchers, ParallelTestExecution, PropSpec}

/**
  * @author <a href="camilo.duran@payulatam.com">Camilo Álvarez Durán</a>
  */
@RunWith(classOf[JUnitRunner])
class SeleniumTest extends PropSpec with GeneratorDrivenPropertyChecks with Matchers with ParallelTestExecution {

  property("LocalDateTime conversion is performed correctly") {
    var driver: WebDriver = null
    var baseUrl: String = null
    var acceptNextAlert: Boolean = true
    val verificationErrors: StringBuffer = new StringBuffer
    driver = new FirefoxDriver
    baseUrl = "https://seg.eltiempo.com"
    driver.manage.timeouts.implicitlyWait(30, TimeUnit.SECONDS)
    forAll(Gen.alphaStr, Gen.alphaStr, Gen.alphaStr) {
      (user: String, email: String, pwd: String) => {
        driver.get(baseUrl + "/eltiempo/user/add")
        driver.findElement(By.id("noSuscriptor")).click
        driver.findElement(By.id("first_name")).clear
        driver.findElement(By.id("first_name")).sendKeys(user)
        driver.findElement(By.id("email")).clear
        driver.findElement(By.id("email")).sendKeys(email + "@gmail.com")
        driver.findElement(By.id("password")).clear
        driver.findElement(By.id("password")).sendKeys("123456")
        driver.findElement(By.name("password_repeat")).clear
        driver.findElement(By.name("password_repeat")).sendKeys("123456")
        driver.findElement(By.id("terms")).click
        driver.findElement(By.id("submitbutton")).click
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.id("b1")))

      }
    }
    driver.quit
    val verificationErrorString: String = verificationErrors.toString
    if ("" != verificationErrorString) {
      fail(verificationErrorString)
    }

  }

}
