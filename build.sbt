name := "TestWeb"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.postgresql" % "postgresql" % "9.3-1104-jdbc41",
  "org.scalatestplus" %% "play" % "1.4.0" % "test",
  "org.scalacheck" %% "scalacheck" % "1.12.5" % "test",
  "ru.yandex.qatools.embed" % "postgresql-embedded" % "1.7" % "test",
  "org.seleniumhq.selenium" % "selenium-java" % "2.35.0" % "test",
  "org.scalatest" % "scalatest_2.10" % "2.0" % "test"
)